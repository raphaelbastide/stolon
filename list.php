<?php require("functions.php"); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Stem List | Stolon</title>
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="font/stylesheet.css">
  <link rel="stylesheet" href="css/main.css">
  <link href='img/favicon.png' rel='icon' type='image/png'>
  </head>
  <body>
    <header>
      <a class="button" href="./">new</a>
      <p><a class="button" href="https://gitlab.com/raphaelbastide/stolon">Install Stolon</a> on your server to host your own data.</p>
    </header>
    <main class="list">
    <?php
    $directory = "stems";
    $rglob_stems = rglob($directory."/{stem.json}", GLOB_BRACE);
    $rglob_stems = array_reverse($rglob_stems);
    
    if (!empty($rglob_stems)) :
      
      // rgblob over stems dir to pouplate array with json data
      // performance is not good since accumulated json contains a lot of text…
      // maybe we should cache a stems.json with only dirs and dates 
      $stems = [];
      foreach ($rglob_stems as $s) {
        $json_data = file_get_contents($s);
        $j = json_decode($json_data, true);
        array_push($stems, $j);
      }

      // sort by date (updated first then newer first)
      function cmp($a, $b){
        $date_format = 'Y-m-d\TH:i:sP'; // ISO 8601 format
        
        // old stems have creation date as timestamp format, not ISO 8601
        $date_a = is_numeric($a['creation_date']) ? DateTime::createFromFormat($date_format, $a["creation_date"]) : $a["creation_date"];
        $date_b = is_numeric($b['creation_date']) ? DateTime::createFromFormat($date_format, $b["creation_date"]) : $b["creation_date"];
        
        if (isset($b["update_date"]) ) {
          $date_b = $b["update_date"];
        }
        if (isset($a["update_date"]) ) {
          $date_a = $a["update_date"];
        }
            
        if ($date_a && $date_b) {
          return $date_b <=> $date_a; // Compare dates
        } else {
          return 0; // or any other appropriate action
        }
    }
      usort($stems, "cmp");
      
      // browse list and display
      foreach ($stems as $stem) :

        echo "\n";        
        $stem_name = $stem['title'];
        $stem_dir = "stems/".$stem_name;
        $parent = $stem['derivated_from'] != "" ? $stem['derivated_from'] : null;
        $thumbpath = $stem_dir . '/stem.webp';
        $thumb = file_exists($thumbpath) ? "<img src='$thumbpath' loading='lazy'>" : "";
        ?>
        <section class='list-item' data-title='<?= $stem_name ?>' <?= $parent != "" ? "data-parent='$parent'" : "" ?> >
          <nav>
            <a class='title' href='<?= $stem_name ?>'><?= $stem_name ?></a>
            <button class='rm-btn button' >×</button>
          </nav>
          <article data-src="<?= $stem_dir ?>/index.html">
            <?= $thumb ?>          
          </article>
        </section>
      <?php endforeach;
    endif ?>    
    </main>

    <script type="text/javascript" src="js/abconnect.js"></script>
    <script type="text/javascript" src="js/cookies.js"></script>
    <script type="text/javascript">
    // Cookies
    if (Cookies.get('cansave')) {
      document.body.dataset.cansave = 1
    }

    const items = document.querySelectorAll('.list-item');

    // remove stem
    const removeBtns = document.querySelectorAll('.rm-btn');
    removeBtns.forEach(btn => {
      btn.onclick = async  () => {
        const confirmation = confirm('Are you sure you want to remove this stem?');
        if(confirmation){
          const item = btn.closest(".list-item")
          const fd = new FormData();
          fd.append("stem", item.dataset.title);
          const response = await fetch("remove.php", {
            method: 'POST',
            body: fd,
          });
          const jsonResponse = await response.json();
          item.remove();
        }
      }
    });

    // blank iframe created in js (not for every item in html)
    const iframe = document.createElement('iframe');
    iframe.style.display = "none";
    // Preview on hover only
    for (var i = 0; i < items.length; i++) {
      let stembox = items[i].querySelector("article")
      let url = stembox.getAttribute('data-src')      
      stembox.classList.add("preview-message")
      stembox.onmouseenter = function(){
        stembox.classList.remove("preview-message");
        stembox.appendChild(iframe);
        // append iframe to box
        iframe.style.display = "block";
        iframe.setAttribute('src', url);
      }
      stembox.onmouseleave = function(){
        stembox.classList.add("preview-message");
        iframe.setAttribute('src', "");
        iframe.style.display = "none";
      };
    }

    // Connect boxes
    drawAll()
    function drawAll(){
      items.forEach(item => {
        var parentName = item.dataset.parent;
        if(parentName){
          const parent = document.querySelector('[data-title="'+parentName+'"] ');
          if(parent) abconnect(item,parent);
        }
      });
    }
    // Run the fonction at window resize too
    window.addEventListener("resize",  function(){
      throttle(redraw(), 800)
    });
    function redraw(){
     var lines = document.querySelectorAll('.line')
     for (var i = 0; i < lines.length; i++) {
       lines[i].remove();
     }
     drawAll()
    }
    function throttle(callback, interval) {
      let enableCall = true;
      return function(...args) {
        if (!enableCall) return;
        enableCall = false;
        callback.apply(this, args);
        setTimeout(() => enableCall = true, interval);
      }
    }
    </script>
  </body>
</html>
