<?php

// go to install if config file if not found
if (!file_exists('config.json')) {
  header("Location:" . "install.php");
}

// get config
$json_config_data = file_get_contents("config.json");
$json_config = json_decode($json_config_data, true);

// Start session
session_start();

// when login form has been submited 
if (isset($_POST["stolon_username"]) && !isset($_SESSION["stolon_username"])) {
  $u = $_POST['stolon_username'];
  $p = $_POST['password'];
  // get logins from config.json
  $logins = $json_config["logins"];
  // test logins 
  foreach($logins as $login){
    if(array_key_exists($u, $login)) {
      if(password_verify($p, $login[$u])) {
        $_SESSION["stolon_username"] = $u;
      }
    }
  }
  // set cookie
  if (isset($_SESSION["stolon_username"])) {
    setcookie('cansave', true, time()+3600*24, '/', '', false, false);
  }
  // prevent double post (redirect if not saving)
  if(!isset($_POST["title"])){
    header("Location:" . $_SERVER['REQUEST_URI']);
  }
}

// if logged out
if (isset($_POST["logout"])) {
  session_destroy();
  unset($_SESSION);
  // prevent double post
  header("Location:" . $_SERVER['REQUEST_URI']);
}

// if anonymous
if (!isset($_SESSION["stolon_username"])) { 
  $anonymous = true; 
  setcookie('cansave', '', time()-3600, '/', '', false, false);
} 

// Stolon templates
function getTemplates(){
  global $json_config;
  if(isset($json_config["templates"]) && count($json_config["templates"])){
    return $json_config["templates"];
  } 
}
function getTemplate($dir){
  global $json_config;
  if(isset($json_config["templates"]) && count($json_config["templates"])){
    if($dir == "default"){      
      $template = reset($json_config["templates"]);
    } else {
      $template = $json_config["templates"][$dir];
    }
    return $template;
  } 
}


// Stolon editors themes
$available_themes = glob("js/monaco-themes/*.json");

$themes = array_map(function($t){ 
  return str_replace(".json", "", basename($t)); 
}, $available_themes);

if (isset($_COOKIE["stolon_theme"])) {
  // theme from user cookie
  $my_theme = $_COOKIE["stolon_theme"];
} elseif(isset($json_config["theme"])){
  // theme from global config
  $my_theme = $json_config["theme"];
} else {
  // default theme
  $my_theme = "Twilight";
}


// Recursive Glob function ; Does not support flag GLOB_BRACE
function rglob($pattern, $flags = 0) {
  $files = glob($pattern, $flags);
  foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
    $files = array_merge($files, rglob($dir.'/'.basename($pattern), $flags));
  }
  return $files;
}
  
// Helper to log to file
function stolog($log_msg){
  if (!file_exists("logs")) { mkdir("logs", 0777, true); }
  $log_file = "logs".'/log_' . date('Y-M-d') . '.log';
  file_put_contents($log_file, $log_msg . "\n", FILE_APPEND);
} 

// SlickPassword, modified class used to generate ids

/**
 * @package slickauth
 * @version 0.1.0
 * @author Oliver Dornauf info@phpshaper.com
 * @copyright (C) phpshaper.COM
 * @license GNU/GPLv3, see LICENSE
 * SlickPassword is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 *
 * SlickPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with [PROGRAM]; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */

class SlickPassword {
  // US
  private $consonants_US = array("b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "qu", "r", "s", "t", "v", "w", "x", "z", "ch", "cr", "fr", "nd", "ng", "nk", "nt", "ph", "pr", "rd", "sh", "sl", "sp", "br", "cl", "pl", "pt", "st", "tr", "lt");
  // US
  private $vowels_US = array("a", "e", "i", "o", "u", "y");
  // querty optimized
  private $specialCharacter_US = array("!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "+", "[", "]","<", ">", "/", ";");
  // querty iOS optimized
  private function generate($consonants, $vowels, $specialCharacters, $passwordLength, $numbersCount, $specialCharactersCount) {
    $consonantsLen = count ($consonants) - 1;
    $vowelLen = count($vowels) - 1;
    $specialCharactersLen =  count($specialCharacters) - 1;
    $index = 0;
    $numberOfCharacters = $passwordLength-$numbersCount-$specialCharactersCount;
    $characterGroup1 = "";
    $characterGroup2 = "";
    $characterGroup3 = "";
    while (strlen ($characterGroup1) < $numberOfCharacters) {
      $pos = mt_rand(0,$vowelLen);
      $characterGroup1 = $characterGroup1.$vowels[$pos];
      $pos = mt_rand(0,$consonantsLen);
      $characterGroup1 = $characterGroup1.$consonants[$pos];
    }
    while (strlen ($characterGroup2) < $numbersCount) {
      $pos = mt_rand(0,$consonantsLen);
      $characterGroup2 =  $characterGroup2.mt_rand(0,9);
    }
    while (strlen ($characterGroup3) < $specialCharactersCount) {
      $pos = mt_rand(0, $specialCharactersLen);
      $characterGroup3 = $characterGroup3.$specialCharacters[$pos];
    }
    $permutation = mt_rand(0,5);
    switch ($permutation) {
      case 0: $result = $characterGroup1.$characterGroup2.$characterGroup3;break;
      case 1: $result = $characterGroup1.$characterGroup3.$characterGroup2;break;
      case 2: $result = $characterGroup2.$characterGroup1.$characterGroup3;break;
      case 3: $result = $characterGroup2.$characterGroup3.$characterGroup1;break;
      case 4: $result = $characterGroup3.$characterGroup2.$characterGroup1;break;
      case 5: $result = $characterGroup3.$characterGroup1.$characterGroup2;break;
    }
    return $result;
  }
  public function generateUS($passwordLength, $numbersCount, $specialCharactersCount) {
   return $this->generate($this->consonants_US, $this->vowels_US, $this->specialCharacter_US, $passwordLength, $numbersCount, $specialCharactersCount);
  }
}
