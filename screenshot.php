<?php

// get config
$stolon_config = require('config.php');
require("functions.php");

$response = [
  "success" => 0,
  "auth" => false,
  "messages" => []
];

if (isset($_SESSION["stolon_username"])) {

  $title            = $_POST["title"];
  $project_dir      = "stems/".$title;
  $project_file     = $project_dir."/stem.json";

  if (!empty($_POST) && !empty($_FILES)){

    $response = array();
    $status = 0;

    /* Getting file name */
    $filename = $_FILES['file']['name'];

    // Upload path
    $uploadPath = $project_dir ."/". $filename;
    
    /* Extension */
    $extension = pathinfo($uploadPath,PATHINFO_EXTENSION);
    $extension = strtolower($extension);

    // webp file
    $webp = $project_dir ."/stem.webp";

    // renamed original file
    $renamed = $project_dir ."/stem." . $extension;

    /* Allowed file extensions */
    $allowed_extensions = array("jpg","jpeg","png");

    /* Check file extension */
    if(in_array(strtolower($extension), $allowed_extensions)) {
      
      /* Upload file */
      if(move_uploaded_file($_FILES['file']['tmp_name'],$renamed)){

        // Convert then delete uploaded file, the old school way (when PHP Imagick extension is not active)
        // $convert = '/usr/bin/convert';    
        // $thumb = $convert . ' ' . $renamed . ' -resize 400x225^ -gravity center -quality 50 -extent 400x225 ' . $webp;
        // exec($thumb);
        // unlink($renamed);

        // Convert then delete uploaded file, the Imagick way…
        $imagick = new Imagick();
        $imagick->readImage($renamed);
        $imagick->cropThumbnailImage(400, 225);
        $imagick->writeImage($webp);
        unlink($renamed);

        // Build response
        $status = 1; 
        $response['path'] = $renamed;
        $response['extension'] = $extension;

        $response['messages'][] = "Thumb has been generated";
      } else {
        $response['messages'][] = "Could’nt save file";
      }
    }

    $response['success'] = 1;
    $response['status'] = $status;
    echo json_encode($response);
    exit;
  }

} else{
  $response['success'] = 0;
  $response['messages'][] = "Error: Wrong password, can’t save!";
  echo json_encode($response);
  exit;
  return;
}