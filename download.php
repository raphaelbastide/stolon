<?php
include "functions.php";

// remove previous zip
array_map('unlink', glob("download/*.zip"));

$stem_name = $_GET["stem"];
$files = rglob("stems/".$stem_name."/{*.*}", GLOB_BRACE);
$result = create_zip($files, "download/".$stem_name.'.zip', $stem_name);

/* creates a compressed zip file */
function create_zip($files = array(),$destination = '', $name, $overwrite = false) {
  //if the zip file already exists and overwrite is false, return false
  // if(file_exists($destination) && !$overwrite) {
  //   echo 'A file already exist.';
  //   return false;
  // }
  $valid_files = array();
  //if files were passed in...
  if(is_array($files)) {
    //cycle through each file
    foreach($files as $file) {
      //make sure the file exists
      if(file_exists($file)) {
        $valid_files[] = $file;
      }
    }
  }
  //if we have good files, build the zip
  if(count($valid_files)) {
    $zip = new ZipArchive();
    if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
      return false;
    }
    foreach($valid_files as $file) {
      $zip->addFile($file,$file);
    }
    $title = $name.".zip";
    echo '<p>'.$title.' has been created!</p> Wait a second or <a class="button download" href="download/'.$title.'">download it here</a>.';
    header("Location: download/$title");
    $zip->close();
    return file_exists($destination);
  }else{
    return false;
    echo '<p>Invalid files :/</p>';
  }
}
