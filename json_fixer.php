<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Stolon JSON fixer</title>
    <style>br+br+br{display: none;}</style>
</head>
<body>
    <h1>Stolon JSON fixer</h1>
    
    <p>This function will patch every stem.json. Click on the following link only if you know what you are doing.</p>

    <a href="?update=true">Update all stem.json files</a><br>
    <a href="?dryrun=true">Or preview changes before…</a>
    
    <?php

        if (isset($_GET['update']) && $_GET['update'] === 'true') {
            echo '<p>JSON files updated successfully!</p>';                    
            echo '<pre>';
            echo updateStemFiles(true);
        }

        if(isset($_GET['dryrun']) && $_GET['dryrun'] === 'true'){
            $r = updateStemFiles(false);
            if( !empty( str_replace("<br>", "", $r) ) ){
                echo '<p>These JSON file will be updated: </p>';        
                echo '<pre>';
                echo $r;
            } else {
                echo "<p>No updates needed</p>";
            }
        }             

        function updateStemFiles($update) {
            $stems = glob("stems/*/stem.json");
                
            $verb = $update ? "has been" : "will be";

            $feedback = ""; 
            
            foreach ($stems as $file) {
                
                $jsonContent = file_get_contents($file);
                $data = json_decode($jsonContent, true);
                $title = $data["title"];
                            
                // Add creation date if it doesn’t exist

                if (isset($data['creation_date']) && $data['creation_date'] === 0) {
                    $data['creation_date'] = date('c', strtotime('-1 year'));
                    $feedback .= "$title <strong>creation_date</strong> $verb updated and set as oldie<br>";
                } elseif (isset($data['creation_date']) && is_numeric($data['creation_date'])) {
                    $data['creation_date'] = date('c', $data['creation_date']);
                    $feedback .= "$title <strong>creation_date</strong> $verb updated <br>";
                }

                // update weird var names to camelCase
                $weird_cases = [
                    "htmlPane_size"=> "htmlPaneSize", 
                    "cssPane_size"=> "cssPaneSize", 
                    "jsPane_size"=> "jsPaneSize", 
                    "outputPane_size"=> "outputPaneSize", 
                    "html_pane_size"=> "htmlPaneSize", 
                    "css_pane_size"=> "cssPaneSize", 
                    "js_pane_size"=> "jsPaneSize",  
                    "output_pane_size"=> "outputPaneSize", 
                    "font_size"=> "fontSize", 
                    "fontsize"=> "fontSize", 
                    'live_reload'=> 'liveReload', 
                    'layout_mode'=> 'layoutMode', 
                ];
                foreach ($weird_cases as $old => $new) {
                    if (isset($data['config'][$old] )) {
                        $data['config'][$new] = $data['config'][$old];
                        unset($data['config'][$old]);
                        $feedback .= "$title <strong>$old</strong> $verb updated to $new: " . $data['config'][$new] . "<br>";
                    }
                }


                // add config keys for old stems

                if ( !isset($data['config']['live_reload']) && !isset($data['config']['liveReload']) ){
                    $data['config']["htmlPaneSize"]= 1;
                    $data['config']["cssPaneSize"]= 1;
                    $data['config']["jsPaneSize"]= 1;
                    $data['config']["outputPaneSize"]= 1;
                    $data['config']["fontSize"]= 15;
                    $data['config']["liveReload"]= false;
                    $data['config']["layoutMode"]= "vertical";
                    $data['config']["panes"]["html"]= true;
                    $data['config']["panes"]["css"]= true;
                    $data['config']["panes"]["js"]= true;
                    $data['config']["panes"]["output"]= true;
                    $feedback .= "$title <strong>config</strong> $verb updated <br>";
                }

                // disable js pane if no js
                if ($data['js'] == "" && $data['config']["jsPane_size"] != 0) {
                    $data['config']["panes"]["js"]= false;
                    $data['config']["jsPane_size"]= 0;
                    $feedback .= "$title <strong>js pane</strong> $verb hidden<br>";
                }

                // disable css pane if no css
                if ($data['css'] == "" && $data['config']["cssPane_size"] != 0) {
                    $data['config']["panes"]["css"]= false;
                    $data['config']["cssPane_size"]= 0;
                    $feedback .= "$title <strong>css pane</strong> $verb hidden<br>";
                }

                // remove url
                if (isset($data['url'])) {
                    unset($data['url']);
                    $feedback .= "$title <strong>url</strong> $verb removed<br>";
                }

                // remove unused keys (snaps, creator_key)

                if (isset($data['snaps'])) {
                    unset($data['snaps']);
                    $feedback .= "$title <strong>snaps</strong> $verb removed <br>";
                }

                // remove live reload at object’s root

                if (isset($data['live_reload'])) {
                    unset($data['live_reload']);
                    $feedback .= "$title <strong>live_reload</strong> $verb removed <br>";
                }

                // remove snap mode and vresion

                if (isset($data['snap_mode'])) {
                    unset($data['snap_mode']);
                    $feedback .= "$title <strong>snap_mode</strong> $verb removed <br>";
                } 
                if (isset($data['version'])) {
                    unset($data['version']);
                    $feedback .= "$title <strong>version</strong> $verb removed <br>";
                }

                // remove creator_key
                if (isset($data['creator_key'])) {
                    unset($data['creator_key']);
                    $feedback .= "$title <strong>creator_key</strong> $verb removed <br>";
                }

                if($update) file_put_contents($file, json_encode($data, JSON_PRETTY_PRINT));
                $feedback .= "<br>";
            }

            return $feedback;
        }

    ?>
</body>
</html>
