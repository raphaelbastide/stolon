# Stolon

Lightweight HTML, CSS, JS web code playground build with PHP. It generates flat files code prototypes that can be shared.

![Screenshot](img/screenshot.png)

## Demo

http://stolon.raphaelbastide.com/  

## Features

- Flatfile structure (no database)
- Forkable, trackable parentship
- Direct access to a custom named stem with `/customname`
- Live preview as you type
- Code can be downloaded and shared easily
- Gallery view
- Font size slider

## Before you install

This app let users write files on your server. I don’t know yet if this app is secure enough for production. Use at your own risks.

## Setup

- You will need PHP 7+
- Copy or clone this repository on your server
- Make sure `.htaccess` is present, and that `RewriteBase` is heading to your stolon directory
- Launch the install script by visiting http://localhost/stolon/
- `stems/` and `download/` directories will be created


## Terminology

- Stolon: the app
- Stems: the directories with HTML, CSS, JS codes
- Fork: copy of a directory (stem) that can be derivated

![Plant](img/plant.svg)

“In biology, stolons (from Latin stolō "branch"), also known as runners, are horizontal connections between organisms. They may be part of the organism, or of its skeleton; typically, animal stolons are external skeletons.” [Wikipedia](https://en.wikipedia.org/wiki/Stolon)

## Stolon Screenshots

[Stolon screenshots](https://codeberg.org/julienbidoret/stolonscreenshot) is a node.js script to generate screenshots for each stem of your stolon instance, it might be helpful if you want a fancy gallery with previews.

## Inspirations

- https://developer.mozilla.org/en-US/blog/introducing-the-mdn-playground
- https://webmaker.app/
- https://slingcode.net
- https://jsbin.com
- https://jsfiddle.net
- https://codesandbox.io
- https://github.com/egoist/codepan

## License

[GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.html)
