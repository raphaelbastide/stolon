# Stolon templates

Templaces should be placed here and declared in `config.php`.

```
"templates" => [
  "Web" => "templates/web",
  "P5.js" => "templates/p5",
  "Paged.js" => "templates/paged",
]
```

They consist in a json file that is used as a base for stem initialisation.

The `libs` directory can be used to store extra files that will be made available in the stem:
- whether for a new stem (they don’t exist in the stem folder and are served by `.htaccess` redirect)
- or for an existing one (they are copied in the stem directory, thanks to the `libs` entry in the json file)