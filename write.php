<?php

// get functions
require("functions.php");

$title            = $_POST["title"];
$is_fork          = isset($_POST["fork"]) ? true : false;
$original_title   = $_POST["original_title"];
$html_content     = $_POST["html_content"];
$css_content      = $_POST["css_content"];
$js_content       = $_POST["js_content"];
$derivated_from   = $_POST["derivated_from"];
$live_reload      = $_POST["live_reload"];
$config           = $_POST["config"];
$libs             = $_POST["libs"];
$project_dir      = "stems/".$title;
$project_file     = $project_dir."/stem.json";

$debug = false;

$response = [
  "success" => 0,
  "auth" => false,
  "messages" => []
];

// authenticate
if (isset($_SESSION["stolon_username"])) { 
  $response["auth"] = true;
} else {
  // check if authentication has been sent with stem data
  // @todo : find a way to not duplicate authentication logic (in functions.php AND write.php)
  $json_config_data = file_get_contents("config.json");
  $json_config = json_decode($json_config_data, true);

  if(isset($_POST["stolon_username"])){
    $u = $_POST['stolon_username'];
    $p = $_POST['password'];
    // get logins from config.json
    $logins = $json_config["logins"];
    // test logins 
    foreach($logins as $login){
      if(array_key_exists($u, $login)) {
        if(password_verify($p, $login[$u])) {
          $_SESSION["stolon_username"] = $u;
          $response["auth"] = true;
        }
      }
    }
  } else {
    $response["success"] = 0;
    $response["messages"][] = "Error: Wrong password, can’t save!";
    return;
  }
}


// Rename stem (if not fork)
// If $original_title != $title, rename original dir before anything else
if($original_title != $title && !$is_fork){
  $response["messages"][] = "Renaming stem";
  $original_project_dir = "stems/".$original_title;
  // check that the new dir doesnt exist before renaming
  if (is_dir($project_dir)) {
    $response["messages"][] = "Stem dirname exists";
    $title = $title . "_";
    $project_dir      = "stems/".$title;
    $project_file     = $project_dir."/stem.json";
    $response["messages"][] = "New stem title is $title";
  }
  // rename
  try {
    rename($original_project_dir, $project_dir);  
    $response["redirect"] = $title;
    $response["message"][] = "Redirect to $title";
  } catch (Exception $e) {
    $response["message"][] = "Error " . $e->getMessage();
  }

  // browse all stems to update every reference to $original_title
  // could be avoided if stem had unique id
  try {
    foreach (glob("stems/*/stem.json") as $s) {
      $content = file_get_contents($s);
      $lookup = '"derivated_from": "' . $original_title;
      if( strpos($content, $lookup) !== false ) {
        $replacement = '"derivated_from": "' . $title;
        $content = str_replace($lookup, $replacement, $content);
        file_put_contents($s, $content);
      }
    }    
  } catch (Exception $e) {
    $response["message"][] = "Error " . $e->getMessage();
  }  
}

// check existing of “stems” directory
if(!is_dir("stems")){
  $response["messages"][] = "/stems directory must be created";
  echo json_encode($response);
  exit();
}

if (!is_dir($project_dir)) {
  $response["messages"][] = "Creating $project_dir";
  if($derivated_from) {
    $response["messages"][] = "Forked from $derivated_from";
  }
  mkdir($project_dir, 0755);
  $code = fopen($project_file, 'w');
  fwrite($code, "");
  fclose($code);
  if(!$debug){
    $response["messages"][] = "Redirect to $title";
    $response["redirect"] = $title;
  }else {
    $json_data = file_get_contents($project_file);
    $j = json_decode($json_data, true);
  }
}

$json_data = file_get_contents($project_file);
$j = json_decode($json_data, true);

$creation_date = isset($j['creation_date']) ? $j['creation_date'] : date('c',time());
$update_date = date('c',time());

$html_file = $project_dir."/index.html";
$js_file = $project_dir."/js/main.js";
$css_file = $project_dir."/css/main.css";

$config = isset($config) ? json_decode($config) : null;

$fields_data = ['title' => $title, 'creation_date' => $creation_date, 'update_date' => $update_date, 'derivated_from' => $derivated_from, 'html' => $html_content, 'css' => $css_content, 'js' => $js_content, 'config' => $config, ];

if(isset($libs)) {
  $libs = explode(",",$libs);
  $fields_data["libs"] = $libs; 
}

$comment = isset($_POST["comment"]) ? $_POST["comment"] : (isset($j['comment']) ? $j['comment'] : null);
if(isset($comment)) {
  $fields_data["comment"] = $comment; 
}

$output = json_encode($fields_data, JSON_PRETTY_PRINT); // Legible Json option

// write json
$fh = fopen($project_file, 'w') or die("can't open file");
fopen($project_file, 'w');
fwrite($fh, $output);
fclose($fh);

// write index
$html = fopen($html_file, "w");
fwrite($html, $html_content);
fclose($html);

// write CSS
if (!file_exists($css_file)) {
  mkdir($project_dir."/css", 0755);
  $css = fopen($css_file, "x");
}else {
  $css = fopen($css_file, "w");
};
fwrite($css, $css_content);
fclose($css);

// write JS
if (!file_exists($js_file)) {
  mkdir($project_dir."/js", 0755);
  $js = fopen($js_file, "x");
}else {
  $js = fopen($js_file, "w");
};
fwrite($js, $js_content);
fclose($js);

// handle libs copy
if(isset($libs)) {
  $libsdir = $project_dir . "/libs";
  // create "libs" dir if needed
  if (!file_exists( $libsdir )) {
    mkdir($libsdir, 0777, true);
  }
  // copy each lib
  foreach ($libs as $lib) {
    
    $libfile = "templates/".$lib;
    $newfile = $project_dir."/".$lib;
    if (!file_exists( $newfile )) { 
      $response["messages"][] = "Copy $lib";
      if (!copy($libfile, $newfile)) {
        $response["messages"][] = "Failed to copy $libfile";
      }       
    }
  }
}

// final feedback
$response["success"] = 1;
$response["messages"][] = "Stem $title is saved";

echo json_encode($response);

// logger

stolog($_SESSION["stolon_username"] . " saved $title " . date('d/m/Y H:i'));

?>
